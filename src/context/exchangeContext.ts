import { createContext } from "react";
import { Rates } from "interfaces";

export const ExchangeContext = createContext<Rates | null>(null);
