import React, { useContext } from "react";
import { ExchangeContext } from "context/exchangeContext";
import { Currency, Rates } from "interfaces";

const Currencies: React.FC = () => {
  const rate = useContext<Rates | null>(ExchangeContext);

  if (!rate) return null;

  if (rate.error) return <div>{rate.error}</div>;

  return (
    <div>
      <p>Курс USD</p>
      Продай по <b>{(rate.usd as Currency).buy.toFixed(2)}</b> <br /> Купи по{" "}
      <b>{(rate.usd as Currency).sell.toFixed(2)}</b>
      <hr />
      <p>Курс EUR</p>
      Продай по <b>{(rate.eur as Currency).buy.toFixed(2)}</b> <br /> Купи по{" "}
      <b>{(rate.eur as Currency).sell.toFixed(2)}</b>
    </div>
  );
};

export default Currencies;
