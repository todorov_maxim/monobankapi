import React, { useState, useMemo, useEffect } from "react";
import { HomePageContainer, Title, Content, Information } from "./style";
import { ExchangeContext } from "context/exchangeContext";
import { getCurrencies } from "action/currencies";
import Currencies from "components/homePage/currencies";
import MonobankCat from "assets/monobankCat.png";
import { Rates } from "interfaces";

const HomePage: React.FC = () => {
  const [rates, setRates] = useState<Rates | null>(null);
  const providerValue = useMemo<Rates | null>(() => rates, [rates]);

  useEffect(() => {
    let isCancelled = false;
    getCurrencies()
      .then(rates => {
        if (isCancelled) {
          return;
        }
        setRates(rates);
      })
      .catch(err => {
        setRates({ error: err.message });
      });

    return () => {
      isCancelled = true;
    };
  }, [setRates]);

  return (
    <ExchangeContext.Provider value={providerValue}>
      <HomePageContainer>
        <Title>Мы приветсвуем тебя в нашем кошачьем обменнике!</Title>
        <Content>
          <img src={MonobankCat} alt="" />
          <Information>
            <Currencies />
          </Information>
        </Content>
      </HomePageContainer>
    </ExchangeContext.Provider>
  );
};

export default HomePage;
