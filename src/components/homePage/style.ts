import styled from "styled-components";

export const HomePageContainer = styled.div`
  display: flex;
  align-items: center;
  flex-direction: column;
`;

export const Content = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-around;
  width: 100%;
  @media (max-width: 768px) {
    flex-direction: column;
  }
`;

export const Title = styled.h2`
  text-align: center;
  @media (max-width: 768px) {
    padding: 10px;
  }
`;

export const Information = styled.div`
  font-size: 30px;
  text-align: center;
  @media (max-width: 768px) {
    font-size: 22px;
    padding: 10px;
  }
`;
