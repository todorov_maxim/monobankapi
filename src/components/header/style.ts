import styled from "styled-components";

export const HeaderContainer = styled.header`
  padding: 30px;
  background: #1d1717;
  font-size: 24px;
  display: flex;
  align-items: center;
  justify-content: space-between;
  @media (max-width: 768px) {
    font-size: 16px;
  }
`;

export const BoldTitle = styled.span`
  font-weight: 700;
  margin-right: 5px;
  font-size: 30px;
  @media (max-width: 768px) {
    font-size: 18px;
    margin-right: 2px;
  }
`;
