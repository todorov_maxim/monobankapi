import React from "react";
import { HeaderContainer, BoldTitle } from "./style";

const Header: React.FC = () => {
  return (
    <HeaderContainer>
      <div>
        <BoldTitle>monobank</BoldTitle> | Universal Bank
      </div>
    </HeaderContainer>
  );
};

export default Header;
