import React from "react";
import { NotFoundPageContainer, NotFoundPageTitle } from "./style";
import ErrorCat from "assets/errorCat.png";
import { Link } from "react-router-dom";

const NotFoundPage: React.FC = () => {
  return (
    <NotFoundPageContainer>
      <img src={ErrorCat} alt="ErrorCat" />
      <Link to="/">
        <NotFoundPageTitle>Вернуться на главную</NotFoundPageTitle>
      </Link>
    </NotFoundPageContainer>
  );
};

export default NotFoundPage;
