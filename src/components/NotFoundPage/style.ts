import styled from "styled-components";

export const NotFoundPageContainer = styled.div`
  display: flex;
  align-items: center;
  flex-direction: column;
  padding: 30px;
`;

export const NotFoundPageTitle = styled.p`
  color: white;
  underline: 1px solid white;
  font-size: 22px;
`;
