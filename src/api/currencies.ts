import { monobankApiUrl } from "constants/urls";
import { IResponseSuccess, IResponseError } from "interfaces";

export const getCurrenciesData = (): Promise<
  IResponseSuccess[] | IResponseError
> => {
  const query = monobankApiUrl + "/bank/currency";

  return fetch(query, {
    method: "GET"
  }).then(
    response => {
      return response.json();
    },
    err => {
      console.log("Error", err);
    }
  );
};
