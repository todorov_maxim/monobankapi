import { getCurrenciesData } from "api/currencies";
import { USD, EUR, UAH } from "constants/isoCodes";
import { Rates } from "interfaces";

export const getCurrencies = (): Promise<Rates> => {
  return getCurrenciesData().then(data => {
    if (Array.isArray(data)) {
      const usd = data.find(
        currency =>
          currency.currencyCodeA === USD && currency.currencyCodeB === UAH
      );
      const eur = data.find(
        currency =>
          currency.currencyCodeA === EUR && currency.currencyCodeB === UAH
      );
      if (usd && eur) {
        const rates = {
          usd: {
            buy: usd.rateBuy,
            sell: usd.rateSell
          },
          eur: {
            buy: eur.rateBuy,
            sell: eur.rateSell
          }
        };

        return rates;
      }

      throw new Error("Упс... Валютная пара не найдена");
    } else {
      throw new Error("Упс... Попробуйте обновить страницу");
    }
  });
};
