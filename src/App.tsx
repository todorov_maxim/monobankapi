import React from "react";
import { Switch, BrowserRouter as Router } from "react-router-dom";
import { routes } from "routes";
import Header from "components/header";

const App: React.FC = () => {
  return (
    <Router>
      <Header />
      <Switch>{routes}</Switch>
    </Router>
  );
};

export default App;
