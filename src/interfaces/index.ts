export interface Currency {
  buy: number;
  sell: number;
}

export interface Rates {
  usd?: Currency;
  eur?: Currency;
  error?: string;
}

export interface IResponseSuccess {
  currencyCodeA: number;
  currencyCodeB: number;
  date: number;
  rateSell: number;
  rateBuy: number;
  rateCross: number;
}

export interface IResponseError {
  errorDescription: string;
}
