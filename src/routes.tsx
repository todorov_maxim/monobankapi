import React from "react";
import { Route } from "react-router-dom";
import HomePage from "components/homePage";
import NotFoundPage from "components/NotFoundPage";
import ErrorBoundary from "components/errorBoundaries";

export const routes = [
  <Route
    key="HomePage"
    exact
    path="/"
    render={() => (
      <ErrorBoundary>
        <HomePage />
      </ErrorBoundary>
    )}
  />,
  <Route key="NotFoundPage" path="*" component={NotFoundPage} />
];
